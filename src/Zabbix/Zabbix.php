<?php

namespace Zabbix;

/**
 * Main Zabbix object
 */
class Zabbix {

	/** Debug mode @var bool */
	private $debug = false;

	/** Extra HTTP headers @var string */
	private $extraHeaders = '';

	/** Api url @var string */
	private $apiUrl = '';

	/** User @var string */
	private $user  = '';

	/** Password @var string */
	private $password  = '';

	/** Token for request @var sring */
	private $authToken = '';

	/** Additional stream context @var array */
	private $streamAddContext = array();

	/**
	 * Creates the Zabbix library
	 *
	 * @param string $apiUrl Api url (http://company.com/zabbix/api_jsonrpc.php)
	 * @param string $user Username API
	 * @param string $password Password API
	 * @param string $httpUser Username for HTTP basic authorization
	 * @param string $httpPassword Password for HTTP basic authorization
	 * @param string $authToken Authentification token, when isset - ignored $user and $password  for request and not cached to file
	 * @param array|null $streamAddContext Config array for connection. E.g, array("ssl"=>array("verify_peer"=>false,"verify_peer_name"=>false))
	 */
	public function __construct(string $apiUrl, string $user = '', string $password = '', string $httpUser = '', string $httpPassword = '', string $authToken = '', array $streamAddContext = null)
	{
		// api url for request
		$this->apiUrl = $apiUrl;

		// Set http authorization
		if ($httpUser && $httpPassword) {
			$this->extraHeaders = 'Authorization: Basic '.base64_encode($httpUser.':'.$httpPassword);
		}

		// user and password for request
		$this->user = $user;
		$this->password = $password;

		// set ssl check for request
		if ($streamAddContext) {
			$this->streamAddContext = $streamAddContext;
		}

		// set toke for request by username-password, or token(prior)
		$this->getAuthToken($authToken);
	}

	/**
	 * This method allows to log in to the API and generate an authentication token, if no exist.
	 * 
	 * NOTE: When using this method, you also need to do user.logout to prevent the generation of a large number of open session records. 
	 *
	 * When a user is successfully logged in for the first time, the token will
	 * be cached / stored in the $tokenCacheFile. For every future
	 * request, the cached auth token will automatically be loaded and the
	 * user.login is skipped. If the auth token is invalid/expired, user.login
	 * will be executed, and the auth token will be cached again
	 *
	 * @param string $authToken Existed token
	 *
	 * @throws Exception
	 *
	 * @return string
	 */
	public function getAuthToken(string $authToken = null)
	{
		if($authToken){

			$this->authToken = $authToken;

			// check token from request with dummy request
			try {
				$this->request('user.get',array('countOutput'=>true));
			} catch (\Exception $e) {
				$this->authToken = '';
			}

		}

		if(!$authToken || $this->authToken == ''){

			// reset auth token
			$this->authToken = '';

			// cache file for token
			$tokenCacheDir = '/tmp/';
			$tokenCacheFile = $tokenCacheDir.'.zabbix-token-'.md5($this->user);

			if(is_file($tokenCacheFile)){

				// get auth token from cached file
				try {
					
					$this->authToken = file_get_contents($tokenCacheFile);
					// make dummy request - take only count for user for check token from file
					$this->request('user.get',array('countOutput'=>true));

				} catch (\Exception $e) {

					// if error (expired token) - unset token
					$this->authToken = '';
					unlink($tokenCacheFile);

				}

			}

			// no exists cached token
			if (!$this->authToken) {
				
				// login and get auth token
				$this->authToken = $this->request('user.login', ['user'=>$this->user,'password'=>$this->password], false);

				if(is_dir($tokenCacheDir)){
					file_put_contents($tokenCacheFile, $this->authToken);
					chmod($tokenCacheFile, 0600);
				}

			}

		}

		// debug - print auth token
		if($this->debug){
			echo 'Auth token is: '.$this->authToken."\r\n";
		}

		return $this->authToken;
	}

	/**
	 * Logout from the API.
	 *
	 * This method allows to log out of the API and invalidates the current authentication token. 
	 *
	 * @param string $authToken Existed token
	 *
	 * @throws Exception
	 *
	 * @return mixed
	 */
	final public function userLogout($authToken)
	{
		$response = $this->request('user.logout', array(), $authToken);

		$this->authToken = '';

		return $response;
	}

	/**
	 * Make request for Zabbix api
	 *
	 * @param string $method https://www.zabbix.com/documentation
	 * @param array $params params for request
	 * @param boolean $withAuth need token to request
	 */
	public function request(string $method, array $params = [], $withAuth = true){

		$this->request = array(
			'jsonrpc' => '2.0',
			'method' => $method,
			'params' => $params,
			'id' => number_format(microtime(true), 4, '', '')
		);

		// add auth token if required
		if($withAuth){
			$this->request['auth'] = $this->authToken ?: null;
		}

		// encode request data
		$this->requestEncoded = json_encode($this->request);

		// debug - print request data
		if($this->debug){
			echo 'Make request: '.$this->requestEncoded."\r\n";
		}

		// tech data for request
		$context = array(
			'http' => array(
				'method' => 'POST',
				'header' => 'Content-type: application/json-rpc'."\r\n".$this->extraHeaders,
				'content' => $this->requestEncoded,
			)
		);

		// Set additional context for request
		if ($this->streamAddContext) {
			foreach($this->streamAddContext as $key=>$value) {
				if(isset($context[$key])){
					$context[$key] = array_merge($context[$key],$value);
				} else {
					$context[$key] = $value;
				}
			}
		}

		$streamContext = stream_context_create($context);

		 // handle api url
		$fileHandler = @fopen($this->apiUrl, 'r', false, $streamContext);

		// check exception for fopen
		if(!$fileHandler){
			throw new \Exception('Could not connect to "'.$this->apiUrl.'"');
		}

		// response get
		$response = @stream_get_contents($fileHandler);

		// verify response
		if ($response === false) {
			throw new \Exception('Could not read data from "'.$this->apiUrl.'"');
		}

		// debug - print response data
		if ($this->debug) {
			echo 'Request response: '.$response."\r\n";
		}

		// decode response
		$responseDecoded = json_decode($response);

		if (property_exists($responseDecoded, 'error') && $error = $responseDecoded->error) {
			$message = 'API error';
			if ($error = $responseDecoded->error) {
				$message .= ' '.$error->code.': '.$error->data;
			}
			throw new \Exception($message.'.');
		}

		return $responseDecoded->result;

	}



}