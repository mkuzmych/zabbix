# PHP library for Zabbix api

## Installation

Require this package, with [Composer](https://getcomposer.org/), in the root directory of your project.

```bash
composer require mkuzmych/zabbix
```

## Usage

Use the library with all parameters you need.

```php
$zabbix = new \Zabbix\Zabbix($apiUrl, $user, $password, $httpUser, $httpPassword, $authToken, $streamAddContext)
```

### Authorization by user - password

```php
$zabbix = new \Zabbix\Zabbix('http://company.com/zabbix/api_jsonrpc.php', 'zabbix_user', 'zabbix_password')
```

### Authorization by token

```php
$zabbix = new \Zabbix\Zabbix('http://company.com/zabbix/api_jsonrpc.php', '', '', '', '', '7fbb6b3201d0ea3363a8bd859ae24a05')
```

### Authorization by user - password behind HTTP Basic Authroization with additional stream context

```php
$streamContext = array(
			'ssl' => array( // set ssl skip cert
				"verify_peer"=>false,
				"verify_peer_name"=>false,
			),
			'http' => array( // set proxy
				'proxy' => 'tcp://192.168.10.40:3128',
				'request_fulluri' => true,
			),
		);

$zabbix = new \Zabbix\Zabbix('http://company.com/zabbix/api_jsonrpc.php', 'zabbix_user', 'zabbix_password', 'auth_user', 'auth_password', '', $streamContext)`
```

### Make requests

The API library has a `request` method that takes three parameters. It returns an associative array containing all of the relevant request information.

Name      | Type     | Description
----------|----------|------------
`method`  | string   | Zabbix api method name https://www.zabbix.com/documentation (e.g.: host.get, host.delete)
`params`  | array    | Parameters for request from documentation (e.g.: array( 'with_items' => true, 'selectItems' => true))
`withAuth`| boolean  | Some Api method make without auth parameter

```php
$response = $zabbix->request('host.get', array(
				'with_items' => true,
				'selectItems' => true
			));

echo "<pre>";
print_r($result);
echo "</pre>";
```

### Fetch authentication token for current request

When a user is successfully logged in for the first time, the token will be cached / stored in the cache file /tmp/.zabbix-token-. For every future request, the cached auth token will automatically be loaded and the user.login is skipped. If the auth token is invalid/expired, user.login will be executed, and the auth token will be cached again

```php
$token = $zabbix->getAuthToken();
```

### Check exist token

If the token is active, the same token value specified in the function will be returned.

```php
$token = $zabbix->getAuthToken('7fbb6b3201d0ea3363a8bd859ae24a05');
```

### Invalidate token

Allows to log out of the API and invalidates the current authentication token

```php
$zabbix->userLogout('1fe801802ef13eafe852b3d0e76fe87c');
```

## Troubleshooting

If you have any questions or problems, create a [ticket](https://gitlab.com/mkuzmych/zabbix/-/issues)

## Framework integrations

- **Laravel** - [mkuzmych/zabbixlaravel](https://gitlab.com/mkuzmych/zabbixlaravel)

If you have integrated Zabbix into a popular PHP framework, let us know!
